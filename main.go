package main

import (
	"bufio"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/binance-chain/go-sdk/common/types"
	"github.com/binance-chain/go-sdk/keys"
	"github.com/cosmos/cosmos-sdk/client/input"
	"github.com/cosmos/cosmos-sdk/crypto/keys/mintkey"
	"gitlab.com/thorchain/tss/go-tss/conversion"
)

func main() {
	f := flag.String("file", "", "file name")
	o := flag.String("output", "", "output binance keystore file")
	flag.Parse()
	reader := bufio.NewReader(os.Stdin)
	password, err := input.GetPassword("Please type in your password:", reader)
	if err != nil {
		panic(err)
	}
	content, err := ioutil.ReadFile(*f)
	if err != nil {
		panic(err)
	}

	privKey, _, err := mintkey.UnarmorDecryptPrivKey(string(content), password)
	if err != nil {
		panic(err)
	}
	priKeyRawBytes, err := conversion.GetPriKeyRawBytes(privKey)
	if err != nil {
		panic(err)
	}
	hexPriKey := hex.EncodeToString(priKeyRawBytes)
	fmt.Println("hex encoded private key:", hexPriKey)
	net := os.Getenv("NET")
	if strings.EqualFold(net, "TESTNET") {
		types.Network = types.TestNetwork
	}
	km, err := keys.NewPrivateKeyManager(hexPriKey)
	if err != nil {
		panic(err)
	}
	fmt.Println("BNB address:", km.GetAddr().String())
	if len(*o) > 0 {
		encryptedJson, err := km.ExportAsKeyStore(password)
		if err != nil {
			panic(err)
		}
		buf, err := json.Marshal(encryptedJson)
		if err != nil {
			panic(err)
		}
		if err := ioutil.WriteFile(*o, buf, 0655); err != nil {
			panic(err)
		}
	}
}
