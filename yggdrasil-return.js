const { BncClient } = require("@binance-chain/javascript-sdk")
const Axios = require('axios');

// ADD THESE
const privKey = '' // Run thornode-privateKey-recovery tool
const yggMEMO = 'YGGDRASIL-:1234' // Find memo here: http://<host>:1317/thorchain/queue/outbound after leaving, eg yggdrasil-:XXX

// CHECK THESE
const baseURL = 'http://54.243.127.43'
const portTHOR = '1317'
const pathYgg = 'vaults/yggdrasil'
const pathPoolAddress = 'pool_addresses'
const multiSendFee = 30000 //https://dex.binance.org/api/v1/fees
const bnbNodeURL = 'https://dex-asiapacific.binance.org/'

var nodeAddress_BNB
var vaultAddress
var bnbClient

const getNodeAddress = async () => {
    console.log('CONNECTING TO BINANCE')
    bnbClient = new BncClient(bnbNodeURL);
    await bnbClient.chooseNetwork("mainnet");
    await bnbClient.initChain()
    let account = await bnbClient.setPrivateKey(privKey)
    let accountDetails = await bnbClient.getAccount(account.address)
    let nodeAddress = accountDetails.result.address
    return nodeAddress
}

const getYggdrasilVault = async (nodeBNBaddress) => {
    let resp = await Axios.get(`${baseURL}:${portTHOR}/thorchain/${pathYgg}`)
    let yggdrasilVaults = resp.data
    let nodeVault
    for await (vault of yggdrasilVaults) {
        let addresses = vault.addresses
        let foundNode = addresses.find((x) => x.address === nodeBNBaddress)
        if (foundNode) {
            let nodeKey = addresses.find((x) => x.chain === "THOR")
            console.log(`Found vault for ${nodeBNBaddress}, associated with node ${nodeKey.address}`)
            nodeVault = vault
        }
    }
    return nodeVault
}

const prepareMultisend = async (coins) => {
    let poolResp = await Axios.get(`${baseURL}:${portTHOR}/thorchain/${pathPoolAddress}`)
    vaultAddress = poolResp.data.current[0].address
    let returnCoins = []

    for await (coin of coins) {
        let ticker = coin.asset.substring(4, coin.asset.length)
        if (ticker !== 'BNB' && +coin.amount > 0) {
            returnCoins.push({ "denom": ticker, "amount": +coin.amount / 10 ** 8 })
            console.log({ "denom": ticker, "amount": +coin.amount / 10 ** 8 })
        }
    }
    console.log(`\nOnly ${returnCoins.length + 1} eligible coins to send`)

    let multiSendFees = ((returnCoins.length + 1) * multiSendFee)
    for await (coin of coins) {
        let ticker = coin.asset.substring(4, coin.asset.length)
        if (ticker === 'BNB') {
            console.log(`Multisend Tx Fee to deduct from ${+coin.amount / 10 ** 8} is ${multiSendFees / 10 ** 8} BNB\n`)
            let bnbMinusFees = +coin.amount - multiSendFees
            returnCoins.push({ "denom": ticker, "amount": bnbMinusFees / 10 ** 8 })
            console.log({ "denom": ticker, "amount": bnbMinusFees / 10 ** 8 })
        }
    }
    let txItem = { "to": vaultAddress, "coins": returnCoins }
    return [txItem]
}

const doMultiSend = async (outputs) => {
    let accountDetails = await bnbClient.getAccount(nodeAddress_BNB)
    let sequence = accountDetails.result.sequence;
    console.log(`\nCHECK THESE DETAILS!!!`)
    console.log(`Paying to Asgard ${vaultAddress} from ${nodeAddress_BNB} using memo ${yggMEMO} with ${outputs[0].coins.length} coins`)
    console.log(`\nDRY RUN COMPLETE, UNCOMMENT THE TRANSACTION AND RE-RUN`)

    // UNCOMMENT THIS AFTER DRY RUN
    // #################
    // let tx = await bnbClient.multiSend(nodeAddress_BNB, outputs, yggMEMO, sequence)
    // console.log(`done: https://explorer.binance.org/tx/${tx.result[0].hash}`)
    // #################
}

const main = async () => {
    nodeAddress_BNB = await getNodeAddress()
    console.log(`\nNode BNB Address for privateKey is ${nodeAddress_BNB}`)
    let yggVault = await getYggdrasilVault(nodeAddress_BNB)
    console.log(`\nFound ${yggVault.vault.coins.length} coins for address, value ${+yggVault.total_value / 10 ** 8} RUNE\n`)
    let outputs = await prepareMultisend(yggVault.vault.coins)
    await doMultiSend(outputs)
}

main()